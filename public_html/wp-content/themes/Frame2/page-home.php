<?php 
/**
 * Template Name: Home Page
 */


get_header(); ?>



 <div class="slider">
    <ul class="slides">

        
        <?php
        
          query_posts('post_type=home-slider&posts_per_page=-1');
          while ( have_posts() ) : the_post();


            $slideImage = types_render_field('background-image',array('raw'=>true));
            $slideAlign = types_render_field('alignment',array('raw'=>true));
            $slideContent = types_render_field('slide-content');
 

              echo"<li>
                  <img src='$slideImage'>
                  <div class='caption $slideAlign-align'>
                     $slideContent
                  </div>
                </li>";

          endwhile;
        wp_reset_query();
        
        ?>


    </ul>
  </div>





  <div class="container">
    <div class="section">

		<?php if (have_posts()) : while (have_posts()) : the_post();?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>

	<?php
	

	

?>
<?php


?>

		</div>
	</div>




<?php get_footer(); ?>
