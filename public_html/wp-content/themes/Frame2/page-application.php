<!doctype html>
<?php 
/* Template Name: Branding Application */
wp_head();
$logoImage = "";
$masterDownload = "";
 ?>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/materialize.js"></script>
    <?php $url = get_template_directory_uri(); ?>
    <style type="text/css">
            @font-face {
                font-family: 'Gibson';
                src: url('<?php echo $url ; ?>/fonts/30C04A_4_0.ttf') format('truetype');
                font-style: normal;
                font-weight: 400;
            }

            @font-face {
                font-family: 'Gibson';
                src: url('<?php echo $url ; ?>/fonts/30C04A_5_0.ttf') format('truetype');
                font-style: normal;
                font-weight: 700;
            }

            @font-face {
                font-family: 'Gibson'; 
                src: url('<?php echo $url ; ?>/fonts/30C04A_7_0.ttf') format('truetype');
                font-style: normal;
                font-weight: 300;
            }

            /*-----------------------------

                Branding App Generic Styles

            --------------------------------*/
            .clearfix {
                clear: both;
            }
            .btnd:active,
            .btnd:focus,
            .btnd.active {
              background-image: none;
              outline: 0;
              -webkit-box-shadow: none;
                      box-shadow: none;
            }

            h2 , h3, h4 {
                font-family: 'Gibson';
                font-weight: 700;
                text-transform: uppercase;
                color: #222;
            }
            h2 {
                font-size: 2.2em;
            }
            h3 {
                font-size: 1.5em;
            }
            h4 {
                font-size: 1.2em;
             }

             p {
                font-family: 'Gibson';
                font-weight: 300;
                font-size: 0.8em;
                line-height: 1.5em;
             }

             p strong {
                font-weight: 700;
                font-size: 0.8em;
                text-transform: uppercase;
                margin-top: 0;
             }

             a {
                color: #8bc7ea;
                transition: .2s opacity ease;
             }
             a:hover {
                color: #8bc7ea;
                opacity: 0.6;
                transition: .2s opacity ease;
             }

             .container {
                width: 80%;
             }

             div.vc_column-inner {
                padding-top: 0px !important;
             } 

             .overflow {
                overflow: hidden;
             }

            /*-----------------------------

                Branding App Side Nav

            --------------------------------*/
           
             p {
                margin-top: 0;
             }

            .show-nav {
                left: 0 !important;
                transition: .2s all ease;
            }

            .slide-over {
                right: 0;
                left: auto;
                transition: .2s all ease;
            }

            .slide-over-con .hamburger {
                left: 0px;
            }
            .overlay-click {
                position: fixed;
                height: 100%;
                width: 100%;
                background: #000;
                opacity: 0.6;
                z-index: 9;
                left: -100%;
            }

            .slidenav {
                left: 0;
                
            }
            .hamburger {
                transition: .2s all ease;
                position: absolute;
                left: -40px;
                top: -25px;

            }

            .full-width {
                width: 100% !important;
            }

            div.slide-over-con {
                right: 0;
                left: auto;
                border-right: 40px solid #fff;
                border-left: 0px solid #fff;
                position: fixed;
            }

            .button-container {
                width: 0; 
                height: 0; 
                border-top: 45px solid transparent;
                border-bottom: 45px solid transparent;
                border-left: 30px solid #fff;
                position: fixed;
                top: 30px;
                z-index: 999;
                transition: .2s border-color ease;
            }
            .scrolling {
                border-top: 45px solid transparent ;
                border-bottom: 45px solid transparent ;
                border-left: 30px solid #a2a4a5 ;
                transition: .2s border-color ease;
            }


            .slide-over-con .hamburger-inner, .slide-over-con .hamburger-inner::before, .slide-over-con .hamburger-inner::after {
                background-color: #000 !important ;
                transition: .2s background-color ease;
            }



            .scrolling .hamburger-inner, .scrolling .hamburger-inner::before, .scrolling .hamburger-inner::after {
                background-color: #fff ;
            }


            .hamburger-inner, .hamburger-inner::before, .hamburger-inner::after {
                background-color: #000;
                height: 2px;
                width: 15px;
            }

            .hamburger-box {
                width: 30px;
                height: 10px;
                display: inline-block;
                position: relative;
            }

            .hamburger-inner::after {
                bottom: -5px;
            }

            .hamburger-inner::before {
                top: -5px;
            }

            .branding-head-logo {
                text-align: center;
                margin-top: 2em;
                margin-bottom: 2em;
                padding-right: 20px;
                padding-left: 20px;
                
            }

            .branding-head-logo:after {
                content: "";
                position: relative;
                right: 0;
                left: 0;
                margin: auto;
                width: 100%;
                height: 1px;
                background: #b9b9b9;
                display: block;
                margin-top: 2em;

            }

            .navigation-menu h4 {
                font-size: 1.1em;
            }

            .menu-heading {
                font-weight: bold;
            }

            .menu-heading h4 {
                font-size: 1em;
            }
            .side-nav {
                box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12) !important;
            }
            .side-nav li {
                padding-top: 5px;
                padding-bottom: 5px;
                padding-left: 20px;
                padding-right: 20px;
            }

            .sub-menu {
                padding-left: 20px;
            }

            .sub-menu li {
                line-height: 1.5em;
            }

            .sub-menu li:hover {
                background: #fff;
            }

            .sub-menu a {
                line-height: 1.5em;
                height: auto;
            }

            .navigation-menu .sub-menu {
                display: none;
            }

            .side-nav li:hover, .side-nav li.active {
                transition: 0.5s background-color ease;
            }

            .side-nav li {
                transition: 0.5s background-color ease;
            }


            /*-----------------------------

                Branding App Main Content

            --------------------------------*/
            .introduction-brandstandards p strong{
                text-transform: none;
            }
            main {
                /*display: inline-block;*/
                /*width: 80%;*/
                /*float: right;*/
            }
            .introduction-brandstandards{
                background: #e6e7e8;
            }
            .introduction p{
                font-size: 1em;
            }
            .brand-standards {
                position: relative;
                overflow: hidden;
                border-bottom: 1px solid #fff;
                padding-bottom: 2em;
                padding-top: 2em;
            }
            .brand-standards h2 {
                /*margin-bottom: 0;*/
                color: #fff;
                font-size: 3em;
            }
            .brand-standards a {
                /*position: absolute;
                bottom: 0;
                right: 0;*/
                font-family: 'Gibson' ;
                color: #000;
                font-weight: 300;
            }
            .brand-standards span {
                margin-left: 20px;
                font-size: 2.8em;
                color: #8bc7ea;
            }

            .brand-sections .brand-section-header  {
                background: #e6e7e8;
                margin-bottom: 6%;
            }

            .brand-sections .brand-section-header h2 {
                line-height: 1.5em;
                font-size: 2em;
            }

            .brand-sub-section {
                border-top: 1px solid #e6e7e8;
                padding-top: 4%;
                margin-bottom: 4%;
            }

            footer {
                text-align: center;
                background: #e6e7e8; 
                color: #222;
                padding-top: 0;
            }

            footer p {
                color: #222;
            }

            footer .legal-header h2 {
                line-height: 2em;
                font-size: 2.5em;
            }

            footer .legal-content {
                border-top: 1px solid #fff;
                border-bottom: 1px solid #fff;
                padding-top: 3%;
                padding-bottom: 3%;
            }

            footer .legal-content p{
                width: 90%;
                display: inline-block;
            }

            footer .legal-content p strong {
                font-size: 1.1em;
            }

            footer .legal-content p:first-child{
                font-size: 1.1em;
            }

            footer .legal-content img {
                display: block;
                margin: 0 auto;
                margin-top: 2%;
            }

            footer .trade-mark {
                height: 100px;
                text-align: color
            }

            footer .trade-mark p {
                width: 80%;
                margin: 0 auto;
            }

            .print-logo {
                display: none;
            }
            @media only screen and (min-width: 400px) {

                .hamburger {
                    transition: .2s all ease;
                    position: absolute;
                    left: -40px;
                    top: -35px;
                }
                .button-container {
                        border-top: 50px solid transparent;
                        border-bottom: 50px solid transparent;
                        border-left: 35px solid #fff;
                        position: fixed;
                        top: 30px;
                        z-index: 999;
                        transition: .2s border-color ease;
                }
                .scrolling {
                    border-top: 50px solid transparent ;
                    border-bottom: 50px solid transparent ;
                    border-left: 35px solid #a2a4a5 ;
                    transition: .2s border-color ease;
                }
                
            }

            @media only screen and (min-width: 550px) {
                    .brand-standards h2 {
                        /*margin-bottom: 0;*/
                        color: #fff;
                        font-size: 2.8em;
                        width: 49%;
                        display: inline-block;
                    }

                    .right-aligns{
                        width: 49%;
                        text-align: right;
                        display: inline-block;
                    }


            }



            @media only screen and (min-width: 992px) {
                     p {
                        font-family: 'Gibson';
                        font-weight: 300;
                        font-size: 0.9em;
                        line-height: 1.5em;
                     }

                     p strong {
                        font-weight: 700;
                        font-size: 0.9em;
                        text-transform: uppercase;
                     }
                    main {
                        display: inline-block;
                        width: 75%;
                        float: right;
                    }

                    main .container {
                        width: 80%;
                    }

                    .vc_row{
                        margin-top: 3%;
                    }
                    .branding-head-logo {
                        padding-left: 30px;
                        padding-right: 30px;
                    }
                    .branding-head-logo:after {
                        width: 100%;
                    }

                    .brand-standards h2 {
                        /*margin-bottom: 0;*/
                        color: #fff;
                        font-size: 2.5em;
                    }

                    .side-nav {
                        width: 25%;
                        clear: both;
                        position: fixed;
                        height: 100%;
                    }
                    .side-nav li {
                        padding-left: 30px;
                        padding-right: 30px;
                    }
                    .side-nav.fixed {
                        position: fixed;
                    }

                    .brand-standards h2 {
                        margin: 0;
                        display: inline-block;
                    }
                   
                    .navigation-menu {
                        display: inline-block;
                        width: 100%;
                    }

                    .navigation-menu .container {
                        width: 100%;
                    }

                    footer .legal-content p {
                        width: 65%;
                        display: inline-block;
                    }

            }
            @media only screen and (min-width: 1800px) {
                    .side-nav {
                        width: 15%;
                    }
                    main {
                        width: 85%;        
                    }
                    main .container {
                        width: 70%;
                    }

                    .brand-sections .brand-section-header h2 {
                        line-height: 1.5em;
                        font-size: 2.8em;
                    }

            }

             @media print {
                .side-nav.fixed {
                    display: none;
                }

                main {
                    width: 100%;
                    float: none;
                }

                footer {
                    margin-top: 4cm;
                }

                .button-container {
                    display: none;
                }

                .right-aligns a {
                    display: none;
                }

               

                 .print-logo {
                    display: block;x
                 }

                 .brand-sections  {
                    page-break-before: always;
                 }

                 .brand-sub-section {
                     page-break-after: always;
                 }

                 .brand-sub-section {
                    border-top: none;
                 }

                  .brand-section-header {
                    margin-bottom: 2% !important;
                 }

                .hide-parent {
                    display: none;
                }

                a {
                    color: #000 !important;
                    text-decoration: none !important; 
                }

            }
        </style>
</head>
<!-- NAV BAR / SIDE BAR -->
<div class="container button-container">
        <div class="hamburger hamburger--squeeze hide-on-large-only">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
</div>
<div class="side-nav fixed" id="nav-mobile">
    <div class="branding-head-logo clearfix">
    	<?php if ( have_posts() ) : ?>
    	    <?php while ( have_posts() ) : the_post(); ?>
    	      	<?php $logoImage = types_render_field('home-header-logo',array('raw'=>true)); ?>
                <?php $masterDownload = types_render_field('master-file-download') ?>
    	      	<img src="<?php echo $logoImage ?>">
    	    <?php endwhile; ?>
    	<?php endif; ?>
    </div>
    <ul class="navigation-menu" >
    <!-- BUILDS OUT MAIN NAVIGATION MENU -->
        <?php 
            // BUILDS THE MAIN NAVIGATION HEADINGS
            $custom_terms = get_terms('branding-application-section');
            foreach($custom_terms as $custom_term) {
                wp_reset_query();
                $args = array('post_type' => 'branding-file',
                    'orderby' =>'date',
                    'order' => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'branding-application-section',
                            'field' => 'slug',
                            'terms' => $custom_term->slug,
                        ),
                    ),
                 );
                // BUILDS MAIN NAVIGATION SUB HEADINGS
                 $loop = new WP_Query($args);
                 if($loop->have_posts()) {
                    $sectionTitle = $custom_term->name;
                    $sectionTitle = substr($sectionTitle, 2);
                    echo '<div>';
                    echo '<li class="menu-heading"><h4>'.$sectionTitle.'</h4> </li>';
                    echo'<ul class="sub-menu">';
                    while($loop->have_posts()) : $loop->the_post();
                        $title = get_the_title();
                        $title = str_replace("&","and" ,$title);
                        $title = str_replace("#","s" ,$title);
                        $title = str_replace("38","s" ,$title);
                        $title = str_replace(";","s" ,$title);
                        $title = str_replace("0","s" ,$title);
                        $title = str_replace(" ", "-", $title);
                        echo '<li><a href="#'.$title.'">'.get_the_title().'</a></li>';
                    endwhile;
                    echo'</ul> </div>' ;


                 }wp_reset_postdata();
            }		 
        ?>
    </ul>
</div>
<div class="overlay-click"> 
</div>
<main>
<!-- INTRODUCTION HEADER -->
    <section class="introduction-brandstandards">
    	<div class="container">
        	<?php if ( have_posts() ) : ?>
        	    <?php while ( have_posts() ) : the_post(); ?>
        	      <div class="brand-standards">
                    <?php $logoImage = types_render_field('home-header-logo',array('raw'=>true)); ?>
                    <img class="print-logo" src="<?php echo $logoImage ?>">
                    <h2 class="left-align">Identity Standards</h2>
                    <div class="right-aligns"><a class="right-align" href="<?php echo $masterDownload ?>" download="BrandStandards-Files.zip">Download All Files<span class="ion-ios-download-outline"></span></a></div>
                 </div>	
                  <?php echo the_content(); ?>    
        	    <?php endwhile; ?>
        	<?php endif; wp_reset_postdata(); ?>
    	</div>
    </section>
    <section>
     <?php 
            // BUILDS SECTION HEADINGS
            $custom_terms = get_terms('branding-application-section');
            foreach($custom_terms as $custom_term) {
               
                $args = array('post_type' => 'branding-file',
                    'posts_per_page' => -1,
                    'orderby' =>'date',
                    'order' => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'branding-application-section',
                            'field' => 'slug',
                            'terms' => $custom_term->slug,


                        ),
                    ),
                 );
                  // GET POST TYPE INFO AND OUTPUT 
                 $loop = new WP_Query($args);
                 if($loop->have_posts()) {
                     $sectionTitle = $custom_term->name;
                    $sectionTitle = substr($sectionTitle, 2);
                    echo '<div class="brand-sections">';
                     echo '<div class="brand-section-header">';
                        echo '<div class="container">';
                            echo '<h2>'.$sectionTitle.'</h2>';
                        echo '</div>';
                    echo '</div>';
                    while($loop->have_posts()) : $loop->the_post();
                        $title = get_the_title();
                        $fullTitle = "<h3> ".get_the_title()." </h3>" ;
                        $title = str_replace("&","and" ,$title);
                        $title = str_replace("#","s" ,$title);
                        $title = str_replace("38","s" ,$title);
                        $title = str_replace(";","s" ,$title);
                        $title = str_replace("0","s" ,$title);
                        $title = str_replace(" ", "-", $title);
                        echo '<div class="container"> ';
                            echo "<div id=".$title." class=\"brand-sub-section scrollspy\">";
                                echo $fullTitle;
                                echo the_content();
                            echo '</div>';
                        echo '</div>';
                    endwhile;   
                    echo '</div>' ;


                 }wp_reset_postdata();
            }        
    ?> 
    </section>
    <footer>
        <div class="legal">
            <div class="legal-header">
               <div class="container">
                    <h2>Legal</h2>
                </div>
            </div>
            <div class="legal-content container">
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                       <?php $footerConent = types_render_field('footer-content',array('raw'=>true));  ?>
                       <?php $footerTradeMark = types_render_field('footer-trademark',array('raw'=>true));  ?>
                       <?php echo $footerConent;  ?>
  
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>
            </div>
        </div>
        <div class="trade-mark valign-wrapper">
            <p class="valign"><?php echo $footerTradeMark; ?></p>
        </div>
    </footer>
</main>


<script type="text/javascript">
   
    jQuery(document).ready(function ($) {

         var linkParent = $('.brand-sections a').parent();
         var parentParent = $(linkParent).parent();
         $(parentParent).addClass('hide-parent');

         console.log(linkParent , parentParent);
         $('.menu-heading').click(function(){
             $(this).parent().children('.sub-menu').slideToggle('fast');
         });

         $('.scrollspy').scrollSpy();
         $('.hamburger').click(function(){
            $('.hamburger').toggleClass('is-active');
            $('.side-nav').toggleClass('show-nav');
            $(this).toggleClass('slide-over');
            $('.button-container').toggleClass('slide-over-con');
            $('.overlay-click').toggleClass('slidenav');
            if($('body').hasClass('overflow')) {
                $('body').removeClass('overflow');
            } else {
                $('body').addClass('overflow');
            }
         });

        

   
        
        $('.overlay-click').click(function(){
            if($('.side-nav').hasClass('show-nav')){
               $('.side-nav').toggleClass('show-nav');
               $('.hamburger').toggleClass('slide-over');
               $('.hamburger').removeClass('is-active');
               $('.button-container').toggleClass('slide-over-con');
               $('body').removeClass('overflow');
               $('.overlay-click').toggleClass('slidenav');
            }
        });

       

        $('.sub-menu li').click(function(){

             if($('.side-nav').hasClass('show-nav')){
                
               $('.side-nav').toggleClass('show-nav');
               $('.hamburger').toggleClass('slide-over');
               $('.hamburger').removeClass('is-active');
               $('.button-container').toggleClass('slide-over-con');
               $('.overlay-click').toggleClass('slidenav');
               $('body').removeClass('overflow');
               
            }
        });

        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
            var topTarget = $('.introduction-brandstandards').height();
            if (scroll >= topTarget) {
                $(".button-container").addClass("scrolling");

            } else {
                $(".button-container").removeClass("scrolling");
            }
        });

       

    });

</script>

<?php wp_footer(); ?>