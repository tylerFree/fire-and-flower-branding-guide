<?php get_header(); ?>

	<main role="main">
		<div class="container">

			<!-- section -->
			<section>

				<div class='row'>
					<div class='col l3 m12 s12'>
				
					    <div class="childnav_toggle btn">
					      	<i class="material-icons">menu</i> Pages in this Section
					    </div>
						<div class='childNav'>
							<ul class='submenu'>
								<?php
								if(wp_get_post_parent_id( $post_ID )==0){
									$id = get_the_id($page->ID);
								}else if (wp_get_post_parent_id( wp_get_post_parent_id( $post_ID ) )==0){
									$id =  wp_get_post_parent_id( $post_ID );
								}else if (wp_get_post_parent_id( wp_get_post_parent_id( wp_get_post_parent_id( $post_ID )) )==0){
									$id = wp_get_post_parent_id( wp_get_post_parent_id( $post_ID ) );
								}else{
									$id = wp_get_post_parent_id( wp_get_post_parent_id( wp_get_post_parent_id( $post_ID ) ) );
								}
							
								wp_list_pages("child_of=$id&sort_column=ID&sort_column=menu_order&title_li="); 
								?>
							</ul>
						</div>
					</div>
					<div class='col l9'>
			        	<section id='pageContent'>				
							<?php while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; ?>
						</section>
					</div>
				</div>

			</section>
			<!-- /section -->
		</div>		
	</main>



<?php get_footer(); ?>
