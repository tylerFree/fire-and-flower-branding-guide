<?php get_header(); ?>

	<main role="main">
		<div class="container">

			<div class='row'>
				<div class='col l3 m12 s12'>
					<!-- section -->
					<section>

					<h1><?php _e( 'Archives', 'html5blank' ); ?></h1>

					<?php get_template_part('loop'); ?>

					<?php get_template_part('pagination'); ?>

					</section>
					<!-- /section -->
				</div>
				<div class='col l9'>
					<?php get_sidebar(); ?>
				</div>
			</div>

		</div>
	</main>



<?php get_footer(); ?>
