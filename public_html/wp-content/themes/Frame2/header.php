<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<!--Import Google Icon Font-->
      	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>
	<?php 
	
	   $logo = get_theme_mod( 'themeslug_logo' );

            if($logo != ""){
				$link = get_bloginfo('url');
				$homeLink ="<a id='logo-container' href='$link' class='logoLink'><img src='$logo'></a>";
          
            } else {
				$link = get_bloginfo('url');
				$siteName = get_bloginfo('name');
               $homeLink ="<a id='logo-container' href='$link' class='homeLink'>$siteName</a>";
            }
	
	?>
	
	<!--<div class='navA' style='display:none;'>
		<div class="navbar-fixed">
		  <nav id="navHead" role="navigation">
			<div class="nav-wrapper container"><?php echo $homeLink;?>
				<div id="searchToggle"><i class="material-icons search-open">search</i><i class="material-icons search-close">clear</i></div>
				<div id="searchWrap">
					<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
							<input type="text" value="" name="s" id="s" />
							<input type="submit" id="searchsubmit" value="Search" />
					</form>
				</div>
			
			  <div id="nav-desktop" class="right hide-on-med-and-down">
			  <?php 
		
			  ?>
				<?php html5blank_nav(); ?>
			  </div>

			  <a href="#" data-activates="nav-mobile" class="button-collapse right"><i class="material-icons">menu</i></a>
			</div>
		  </nav>
		</div>
		<div class="nav-wrapper">
			<div id="nav-mobile" class="side-nav">
					<?php html5blank_nav(); ?>
			</div>
	
		</div>
	</div>-->
	
	<div class='navB'>
		<div class='navbar-fixed'>
	
			<nav role='navigation' id='navHead'>
				
				<div class='nav-wrapper container'>
					<div class='mobile nav-actiavator'>
					<button class="hamburger hamburger--spin" type="button">
					  <span class="hamburger-box">
						<span class="hamburger-inner"></span>
					  </span>
					</button>
</div>
			<?php
			
			
         
           
				$navDev = array('menu' => 'Main Nav','fallback_cb' => 'wp_page_menu','items_wrap' => '<div class="leftMain"><div class="homeLink">'.$homeLink.'</div></div><div class="searchWrap"><form role="search" action="'.$action.'" method="get" id="searchform"><input type="text" class="form-control" name="s" placeholder="Search"/><button type="submit" class="form-control" alt="Search" /><i class="material-icons">search</i></button></form></div><div class="mobileWrap"><ul id="%1$s" class="%2$s">%3$s</ul></div><div class="rightMain"><div class="searchIcon"><a href="#" class=" search-act"><i class="material-icons search-open">search</i><i class="material-icons search-close">clear</i></a></div></div>','walker' => new WPSE_78121_Sublevel_Walker); 	
	//	$navDev = array('menu' => 'main menu','fallback_cb' => 'wp_page_menu','items_wrap' => '<div class="leftMain"><div class="homeLink"><a href="'.$home.'">HOME</a></div></div><div class="mobileWrap"><ul id="%1$s" class="%2$s">%3$s<li class="mobile"><div class="socialNav"><a href="#" alt="Instagram Link"><span class="icon-instagram"></span></a><a href="#" alt="Twitter Link"><span class="icon-twitter"></span></a><a href="#" alt="Linkedin Link"><span class="icon-linkedin2"></span></a></div></li></ul></div><div class="rightMain"><div class="socialNav desktop"><a href="#" alt="Instagram Link"><span class="icon-instagram"></span></a><a href="#" alt="Twitter Link"><span class="icon-twitter"></span></a><a href="#" alt="Linkedin Link"><span class="icon-linkedin2"></span></a></div><div class="mobile"><div class="navActivator"><a id="nav-toggle" href="#"><span></span></a></div></div></div>'); 
						//$navDev = array('menu' => 'Main Nav','fallback_cb' => 'wp_page_menu','items_wrap' => '<div class="leftMain"><div class="homeLink">'.$homeLink.'</div></div><div class="searchWrap"><form role="search" action="'.$action.'" method="get" id="searchform"><input type="text" class="form-control" name="s" placeholder="Search"/><button type="submit" class="form-control" alt="Search" />Search</button></form></div><div class="main-nav"><ul id="%1$s" class="%2$s">%3$s</ul></div><div class="rightMain"><div class="searchIcon"><a href="#" class=" search-act"><i class="material-icons search-open">search</i><i class="material-icons search-close">clear</i></a></div></div>');
                        wp_nav_menu($navDev);
						
						
						
			?>
				</div>
			</nav>
		</div>
	</div>


	<?php 
	if(!is_front_page()){

			$headContent = "<h1>".get_the_title()."</h1>";	
						
			if(is_search()){
				$term =  get_search_query();
				$headContent ="<h1>Results for: $term</h1>";
				$headImage = esc_url( get_theme_mod('themeslug_header'));
			}
			if(is_404()){
        		$headContent = "<h1>404 - Page not found</h1>";
      		}
	
		?>



		    <div id="pageHead" style="background:#efefef;">  
		        <div class="container">
		          <div class="intro-wrap row">
		          		<div class="col s12">
				  	   <?php echo $headContent;?>
				  	   </div>

		          </div>
		        </div>

		    </div>
		
		<?php
	}?>
