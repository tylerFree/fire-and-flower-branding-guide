<?php /* Template Name: Portfolio */ get_header(); ?>

	<main role="main">
		<div class="">

			<!-- section -->
			<section>
				<div class="row">
					<div class="col l12">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>










				<?php
				
		  		query_posts('post_type=project&posts_per_page=-1');
					$portArray=array();
					$typeArray=array();
					while ( have_posts() ) : the_post();

						$blogURL = get_bloginfo('template_url');

						$terms = get_the_terms($post->ID,'project-type');

						//print_r(get_the_meta());

						$typeName = '';
						$typeSlug = '';
						foreach($terms as $term):
						
							$typeName .= $term->name.'~';
							$typeSlug .= $term->slug.'~';

						endforeach;//Endforeach $term
						$typeName = rtrim($typeName, "~");
						$typeSlug = rtrim($typeSlug, "~");
						
						$title = get_the_title();
						$copy = get_the_excerpt();
						$copy = strip_tags($copy);
						$copy = substr($copy,0,150);

						$link = get_the_permalink();
						
						if ( has_post_thumbnail() ) {
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
						} else {
							$thumb_url = $blogURL . "/img/thumb.jpg";
						}

						$projectArray[] = array('title'=>$title,'content'=>$copy,'link'=>$link,'image'=>$thumb_url,'typeName'=>$typeName,'typeSlug'=>$typeSlug);
						


					endwhile;
				wp_reset_query();
				?>


				<div class="row">
					<div class="col l12">
						<div class="button-group filter-button-group">
						  <button class="btn" data-filter="*">show all</button>
						  <!-- <button class="btn" data-filter=".website">Websites</button>
						  <button class="btn" data-filter=".branding">Branding</button>
						  <button class="btn" data-filter=".print-materials">Print Materials</button> -->

						  <?php   $terms = get_terms('project-type');
						  		foreach($terms as $term){
						  			$name = $term->name;
						  			$slug = $term->slug;
						  			echo " <button class='btn' data-filter='.$slug'>$name</button>";
						  		}
						  ?>

						</div>
					</div>
				</div>

				<div class="row port-grid">
			  

					<?php
						
						foreach($projectArray as $proj){

							$title= $proj['title'];
							$copy =$proj['content'];
							$link =$proj['link'];
							$image =$proj['image'];
							$typeName =$proj['typeName'];
							$typeSlug = $proj['typeSlug'];

							$tNameAr= explode('~',$typeName);
							$tSlugAr = explode('~',$typeSlug);
							
							$tSlugClean = str_replace('~',' ',$typeSlug);
							echo "
				  							
								
							        <div class='col s12 m6 l4 port-item $tSlugClean'>
							          <div class='card'>
							            <div class='card-image'>
							            	<a href='$link'>
							              		<img src='$image'>
							              	</a>
							            </div>
							            <div class='card-content'>
							            	<h3 class='card-title'>$title</h3>
							            	<div class='proj-type'>";
							            	for($x=0;$x<count($tNameAr);$x++){
							            		echo "<a href='#' data-filter='.$tSlugAr[$x]'>$tNameAr[$x]</a>";
							            		if($x<(count($tNameAr)-1)){
							            			echo " | ";
							            		}
							            	}

							            	echo "</div>
							            	<p>$copy</p>
							            </div>
							            <div class='card-action'>
							              <a href='$link'>Read More</a>
							            </div>
							          </div>
							        </div>
							    ";
						}
					
					?>




				</div>
			</section>
			<!-- /section -->
		</div>		

<?php get_footer(); ?>


