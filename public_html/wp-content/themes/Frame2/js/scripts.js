(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';


    (function($){
      $(function(){

          $('.button-collapse').sideNav({
		      menuWidth: 300, // Default is 240
		      edge: 'right', // Choose the horizontal origin
		      closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
		    }
		  );

      }); // end of document ready
    })(jQuery); // end of jQuery name space


   
    $( "#searchToggle" ).click(function() {
     
     	$(this).toggleClass('open');
    	$( "#searchWrap" ).slideToggle( "fast", function() {
    	});

    	  if($('#searchToggle').hasClass('open')){
    	  	console.log('test');
    	  	$( "#searchform #s" ).focus();
    	  } else {
    	  	$('#searchform #s').val('');
    	  }

    });


	  $(document).ready(function(){
	    $('.scrollspy').scrollSpy();
	  });
      

    // ISOTOPE
    function isoGo(){
      var $grid = $('.port-grid').isotope({
        itemSelector: '.port-item',
        stagger: 60
      });
    }

    $(window).load(function() {
      isoGo();
    });

    $(window).resize(function(){
      isoGo();
    });

    // filter items on button click
    $('.filter-button-group').on( 'click', 'button', function() {
      $('.filter-button-group').find('.active').removeClass('active');
      $(this).addClass('active');
      var filterValue = $(this).attr('data-filter');
      $('.port-grid').isotope({ filter: filterValue });
    });

    $('.filter').on( 'click', function() {
      var filterValue = $(this).attr('data-filter');
      $('.port-grid').isotope({ filter: filterValue });
      console.log(filterValue);
      return false;
    });

    // END ISOTOPE





    //---------- ON DOCUMENT READY  --------------

    $( document ).ready(function() {

    	
 



        $('.slider').slider({
          full_width: true,
          height: 600
        });



        $( ".childNav li.page_item_has_children" ).each(function(){
          $(this).append( "<div class='toggle'></div>" );
        });

        $( ".childNav li.page_item_has_children.current_page_ancestor" ).find( ".toggle" ).addClass("opened");


          $( ".page_item_has_children .toggle" ).click(function (e) {
            e.stopPropagation();
            $(this).toggleClass('opened');
            $(this).siblings('.children').stop().slideToggle( 'fast', function() {
          });

        });




        $( ".side-nav li.menu-item-has-children.current_page_ancestor > .toggle" ).each(function() {
      	    $( this ).addClass( "opened" );
      	});




        $( ".side-nav .menu-item-has-children .toggle" ).click(function (e) {
              e.stopPropagation();
              $(this).toggleClass('opened');
              $(this).siblings('.sub-menu').stop().slideToggle( 'fast');
        });

        $('	')



		// $( ".stretchRow" ).append( "<div class='stretch'></div>" );

		// var bgcol = $('.stretchRight .vc_column-inner').css("background-color");
		// $( ".stretch" ).css( "background-color", bgcol );


    });



    // Hide Header on on scroll down
    (function($){
      $(function(){  

    	var didScroll = false;
        var scroll = $(document).scrollTop();
        var headerHeight = $('#navHead').outerHeight();

    	function scrolldeely(){
          var scrolled = $(document).scrollTop();
          if (scrolled > headerHeight){
            $('#navHead').addClass('off-canvas');
          } else {
            $('#navHead').removeClass('off-canvas');
          }

            if (scrolled > scroll){
             $('#navHead').removeClass('fixed');
            } else {
            $('#navHead').addClass('fixed');
            }             
          scroll = $(document).scrollTop();  
    	}

    	window.onscroll = doThisStuffOnScroll;

    	function doThisStuffOnScroll() {
    	    didScroll = true;
    	}

    	setInterval(function() {
    	    if(didScroll) {
    	        didScroll = false;
    	        scrolldeely();
    	        console.log('You scrolled');
    	    }
    	}, 100);

       });
	   
    })(jQuery); 
    // END Hide Header on on scroll down


		
	});
	
	$( ".slider" ).hover(
	 function() {
	   $( this ).slider('pause');
	 }, function() {
	   $( this ).slider('start');
	 }
	);
	
	jQuery(document).ready(function () {
		var example = $('#menu-main-nav-2').superfish({
			//add options here if required
		});
	    jQuery('div.main-nav').meanmenu();
	});
	
})(jQuery, this);
jQuery(function($) { 
	if($('.navB').is(':visible')){
	
	
			$(".menu-item-has-children").hover(function(){
				if($(window).width()>992){
					var newHeight = $(this).find('.sub-menu-wrap').find('ul.sub-menu').outerHeight(true);
					$(this).find('.sub-menu-wrap:first').stop().animate({
						height:newHeight
					},150);
				}
			},function(){
				if($(window).width()>992){
					$(this).find('.sub-menu-wrap').stop().animate({
						height:0
					},0);
				}
			});
			
		
		
		
		$('.nav-actiavator').click(function(){
			if(!$(this).hasClass("open")){
				var navHeight = $('.mobileWrap').find('ul').outerHeight(true);
				
				$('.mobileWrap').stop().animate({
					height:navHeight
				},150,function(){
					$(this).css({'overflow':'scroll','height':'auto','max-height':($(window).height()-56)});	
				});
				$(this).addClass('open');
				$('body').css('overflow','hidden');
			}else{
				$('.sub-menu-wrap').stop().animate({
					height:0
				},150,function(){
					$(this).removeAttr('style');	
				});
				
				//resetting the toggle function, will reset the toggle so it can be opened again when the main menu is closed
				$('.sub-toggle').unbind('click').toggle(function(){
					var $sub = $(this).siblings('.sub-menu-wrap');
					var subHeight = $sub.children('ul').outerHeight(true);
						$sub.stop().animate({
							height:subHeight
						},150,function(){
							$(this).css('height','auto');	
						});
				
				},function(){
					var $sub = $(this).siblings('.sub-menu-wrap');
						$sub.stop().animate({
							height:0
						},150);
					
				});

					
					
					
					
					
					
					
				$('.mobileWrap').stop().animate({
					height:0
				},150,function(){
					$(this).removeAttr('style');	
				});
				$(this).removeClass("open");
				$('body').css('overflow','auto');
				
			}
		});
		//initial toggle function, will run when the menu is first opened
		$('.sub-toggle').toggle(function(){
			var $sub = $(this).siblings('.sub-menu-wrap');
				var subHeight = $sub.children('ul').outerHeight(true);
						$sub.stop().animate({
							height:subHeight
						},150,function(){
							$(this).css('height','auto');	
						});
				
				},function(){
					var $sub = $(this).siblings('.sub-menu-wrap');
						$sub.stop().animate({
							height:0
						},150);
					
		/*	var $sub = $(this).siblings('.sub-menu-wrap');
			var navBarH = $('.nav-wrapper').outerHeight(true);
			var subHeight = $sub.children('ul').outerHeight(true);
			var navHeight = $('.mobileWrap').children('ul').height();
		
			var totalNav = (subHeight + navHeight);
			
			console.log(subHeight + ' ' + navHeight + ' ' + totalNav);
			
			if($(window).height()<totalNav){
				console.log('over here');
				$sub.stop().animate({
					height:subHeight
				},150,function(){
					$(this).css('height','auto');	
				});
				$('.mobileWrap').stop().animate({
					height:totalNav-112
				},150);
			}else{
				console.log('no here');
				$sub.stop().animate({
					height:subHeight
				},150,function(){
					$(this).css('height','auto');	
				});	
				
				$('.mobileWrap').stop().animate({
					height:($(window).height()-56)
				},150);
			}
			
		},function(){
			var $sub =$(this).siblings('.sub-menu-wrap');
			var navBarH = $('.nav-wrapper').outerHeight(true);
			var subHeight  = $sub.children('ul').outerHeight(true);
			var navHeight =$('.mobileWrap').children('ul').height();
			var totalNav = (navHeight - subHeight);
			console.log(totalNav);
			if($(window).height()<totalNav){
				console.log('this one');
				$sub.stop().animate({
					height:0
				},150);
				$('.mobileWrap').stop().animate({
					height:($(window).height()-navBarH)
				},150);
			}else{
							console.log('that one');
				$sub.stop().animate({
					height:0
				},150);
				$('.mobileWrap').stop().animate({
					height:totalNav
				},150);
		
			}*/
			
		});

		$('.search-act').click(function(){
			$('.search-open').toggle();
			$('.search-close').toggle();
			$('.searchWrap').slideToggle(150);
			if($('.searchWrap').is(":visible")){
				$('.searchWrap').find('input').focus();
				
			}
		});
	}


});

 var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }
