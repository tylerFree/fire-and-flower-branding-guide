<?php /* Template Name: News */ get_header(); ?>

	<main role="main">
		<div class="container">

			<!-- section -->
			<section>



				<?php
				
		  		query_posts('posts_per_page=-1');
					$portArray=array();
					$typeArray=array();

					while ( have_posts() ) : the_post();
						$blogURL = get_bloginfo('template_url');

						$categories = get_the_category();
						$separator = ' ';
						$output = '';
						$catSlug = '';

						if ( ! empty( $categories ) ) {
						    foreach( $categories as $category ) {
						        $output .= '<a class="filter" data-filter=".' . esc_html( $category->slug ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
						        $catSlug .= esc_html( $category->slug ).' ';
						    }
						    $catList =  trim( $output, $separator );
						    
						}
						
						
						$title = get_the_title();
						$copy = get_the_content();
						$copy = strip_tags($copy);
						$copy = preg_replace('/\[.*?\]|/', '', $copy);
						$copy = substr($copy,0,150);

						$link = get_the_permalink();
						
						if ( has_post_thumbnail() ) {
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
						} else {
							$thumb_url = $blogURL . "/img/thumb.jpg";
						}

						$projectArray[] = array('title'=>$title,'content'=>$copy,'link'=>$link,'image'=>$thumb_url,'catlist'=>$catList, 'catslug'=>$catSlug);
						


					endwhile;
				wp_reset_query();
				?>

				<div class="row">
					<div class="col l12">
						<div class="button-group filter-button-group">
						  <button class="btn" data-filter="*">show all</button>
						  <!-- <button class="btn" data-filter=".website">Websites</button>
						  <button class="btn" data-filter=".branding">Branding</button>
						  <button class="btn" data-filter=".print-materials">Print Materials</button> -->
						  <?php   $cats = get_categories();
						  		foreach($cats as $cat){
						  			$name = $cat->name;
						  			$slug = $cat->slug;
						  			echo " <button class='btn filter' data-filter='.$slug'>$name</button>";
						  		}
						  ?>
						</div>
					</div>
				</div>

				<div class="row port-grid">
			  

					<?php
						
						foreach($projectArray as $proj){

							$title= $proj['title'];
							$copy =$proj['content'];
							$link =$proj['link'];
							$image =$proj['image'];
							$catlist =$proj['catlist'];
							$catSlug =$proj['catslug'];

							echo "
							        <div class='col s12 m6 l4 port-item $catSlug'>
							          <div class='card'>
							            <div class='card-image'>
							            	<a href='$link'>
							              		<img src='$image'>
							              	</a>
							            </div>
							            <div class='card-content'>
							            	<h3 class='card-title'>$title</h3>
							            	<div class='proj-type'>$catlist</div>
							            	<p>$copy</p>
							            </div>
							            <div class='card-action'>
							              <a href='$link'>Read More</a>
							            </div>
							          </div>
							        </div>
							    ";
						}
					
					?>





				</div>
			</section>
			<!-- /section -->
		</div>		

<?php get_footer(); ?>


