		  <footer class="page-footer">
		    <div class="container">
		      <div class="row">
		        <div class="col l3 m6 s12">
		          <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-widget-1')) ?>
		        </div>
		        <div class="col l3 m6 s12">
		          <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-widget-2')) ?>
		        </div>
		        <div class="col l3 m6 s12">
		          <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-widget-3')) ?>
		        </div>
		        <div class="col l3 m6 s12">
		          <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-widget-4')) ?>
		        </div>
		      </div>
		    </div>
		    <div class="secondary-footer">
		      <div class="container">
		      		<div class="row">
		      			<div class="col s6 copyright">

							<p>
								&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. 
							</p>
						</div>

						<div class="col s6 social-links">
							<p>
								<a href="#"><i class="icon ion-social-facebook"></i></a> 
								<a href="#"><i class="icon ion-social-twitter"></i></a> 
								<a href="#"><i class="icon ion-social-linkedin"></i></a> 
								<a href="#"><i class="icon ion-social-instagram"></i></a>
							</p>
						</div>
					</div>
		      </div>
		    </div>
		  </footer>






		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
